unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Shape1: TShape;
    Timer1: TTimer;
    procedure Timer1Timer(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    toques, toques2, toques3, toques4:integer;
  end;

var
  Form1: TForm1;
  Topo, direita : boolean;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  if topo=true then
    shape1.Top:=shape1.top-5
  else
      shape1.Top:=shape1.Top+5;

  if direita=true then
    shape1.left:=shape1.left-5
  else
    shape1.left:=shape1.left+5;


  if shape1.top <=0 then
    begin;
    Topo:=false;
    toques:= toques+1;
    end;


  if shape1.top + shape1.height >= form1.height then
      begin;
    Topo:=true;
    toques4:=toques4+1;
      end;


    if shape1.left <=0 then
      begin;
    direita:=false;
    toques2:= toques2+1;
      end;

  if shape1.left + shape1.width >= form1.width then
    begin;
    toques3:= toques2+1;
    direita:=true;
    end;


  label1.caption:= inttostr (toques);
  label2.caption:= inttostr (toques2);
  label3.caption:= inttostr (toques3);
  label4.caption:= inttostr (toques4);

end;

end.

