unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Shape1: TShape;
    Timer1: TTimer;
    Timer2: TTimer;
    procedure Button1Click(Sender: TObject);
    procedure Label10MouseEnter(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    cima, baixo, esquerda, direita1, tempo: integer;
  end;

var
  Form1: TForm1;
  Topo, direita : boolean;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Label10MouseEnter(Sender: TObject);
begin
  label10.caption:= 'Flávio Silva';
end;


procedure TForm1.Timer1Timer(Sender: TObject);
begin
  if topo=true then
    shape1.Top:=shape1.top-5
  else
      shape1.Top:=shape1.Top+5;

  if direita=true then
    shape1.left:=shape1.left-5
  else
    shape1.left:=shape1.left+5;


  if shape1.top <=0 then
    begin;
    Topo:=false;
    cima:=cima+1;
    end;

  if shape1.top + shape1.height >= form1.height then
    begin;
    Topo:=true;
    baixo:=baixo+1;
    end;


    if shape1.left <=0 then
      begin;
    direita:=false;
    direita1:=direita1+1;
      end;

  if shape1.left + shape1.width >= form1.width then
    begin;
    direita:=true;
    esquerda:=esquerda+1;
    end;



  label1.caption:= inttostr (cima);
  label2.caption:= inttostr (esquerda);
  label3.caption:= inttostr (direita1);
  label4.caption:= inttostr (baixo);

end;

procedure TForm1.Timer2Timer(Sender: TObject);
begin
  tempo:=tempo+1;

  label12.caption:= inttostr (tempo);
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  Application.terminate;
end;

end.

