unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Label1: TLabel;
    Label10: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Timer1: TTimer;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure Label8MouseEnter(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    num1, num2, cliques, tempo :integer;
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin

    randomize;

  num1:=random (640);
  button1.top:=num1;

  num2:=random (1110);
  button1.left:=num2;

  cliques:=cliques+1;

  label2.caption:= inttostr (num1);
  label3.caption:= inttostr (num2);
  label7.caption:= inttostr (cliques);

end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  application.terminate;
end;

procedure TForm1.FormClick(Sender: TObject);
begin
  application.messagebox('Fallhaste no botão!', 'Informação');
end;

procedure TForm1.Label8MouseEnter(Sender: TObject);
begin
  label8.caption:= 'Presunto Silva';
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  tempo:=tempo+1;
  label10.caption:= inttostr (tempo);
  if tempo=1 then
   application.messagebox('O objetivo deste jogo é conseguires clicar o maximo de vezes no botão que vai mudando de lugar, cada vez que lhe tocas, por isso clica o maximo de vezes que conseguires, boa sorte!', 'Informação');
end;

end.

