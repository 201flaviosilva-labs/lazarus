unit calculadora2;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    RadioGroup1: TRadioGroup;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);

  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin
     Label3.Caption:=floattostr(strtofloat(edit1.Text)+strtofloat(Edit2.Text));
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Label3.Caption:=floattostr(strtofloat(edit1.Text)/strtofloat(Edit2.Text));
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  Label3.Caption:=floattostr(strtofloat(edit1.Text)-strtofloat(Edit2.Text));
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  Label3.Caption:=floattostr(strtofloat(edit1.Text)*strtofloat(Edit2.Text));
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
     if radiobutton1.checked=true then
     Label3.Caption:=floattostr(strtofloat(edit1.Text)+strtofloat(Edit2.Text))
        else   if radiobutton2.checked=true then
      Label3.Caption:=floattostr(strtofloat(edit1.Text)*strtofloat(Edit2.Text))

      else   if radiobutton3.checked=true then
      Label3.Caption:=floattostr(strtofloat(edit1.Text)-strtofloat(Edit2.Text))

      else   if radiobutton4.checked=true then
      Label3.Caption:=floattostr(strtofloat(edit1.Text)/strtofloat(Edit2.Text));



end;

procedure TForm1.Button6Click(Sender: TObject);
begin
  edit1.clear;
  edit2.clear;
  Label3.caption:= ' ';
end;

procedure TForm1.Button7Click(Sender: TObject);
begin
 Application.Terminate;
end;

end.

