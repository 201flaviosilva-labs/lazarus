unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TCalculadora }

  TCalculadora = class(TForm)
    Button1: TButton;
    Button10: TButton;
    Button11: TButton;
    Button12: TButton;
    Button13: TButton;
    Button14: TButton;
    Button15: TButton;
    Button16: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    Button9: TButton;
    Edit1: TEdit;
    procedure Button10Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure Button12Click(Sender: TObject);
    procedure Button13Click(Sender: TObject);
    procedure Button14Click(Sender: TObject);
    procedure Button15Click(Sender: TObject);
    procedure Button16Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    var n1, n2, res, opr : double;
  end;

var
  Calculadora: TCalculadora;

implementation

{$R *.lfm}

{ TCalculadora }

procedure TCalculadora.Button8Click(Sender: TObject);
begin
  edit1.Text:=edit1.text+'1';

end;

procedure TCalculadora.Button10Click(Sender: TObject);
begin
  edit1.Text:=edit1.text+'3';
end;

procedure TCalculadora.Button11Click(Sender: TObject);
begin
  n2:=strtofloat(edit1.text);
   if opr=4 then
  res:=n1+n2
   else if opr=1 then
  res:=n1/n2
   else if opr=2 then
  res:=n1*n2
   else if opr=3 then
  res:=n1-n2;
  edit1.text:=floattostr(res);
end;

procedure TCalculadora.Button12Click(Sender: TObject);
begin
  edit1.Text:=edit1.text+'0';
end;

procedure TCalculadora.Button13Click(Sender: TObject);
begin
  n1:=strtofloat(edit1.text);
  edit1.clear;
  opr:=4
end;

procedure TCalculadora.Button14Click(Sender: TObject);
begin
     n1:=strtofloat(edit1.text);
  edit1.clear;
  opr:=1
end;

procedure TCalculadora.Button15Click(Sender: TObject);
begin
   n1:=strtofloat(edit1.text);
  edit1.clear;
  opr:=2
end;

procedure TCalculadora.Button16Click(Sender: TObject);
begin
   n1:=strtofloat(edit1.text);
  edit1.clear;
  opr:=3
end;

procedure TCalculadora.Button1Click(Sender: TObject);
begin
  edit1.Text:=edit1.text+'7';
end;

procedure TCalculadora.Button2Click(Sender: TObject);
begin
  edit1.Text:=edit1.text+'8';
end;

procedure TCalculadora.Button3Click(Sender: TObject);
begin
  edit1.Text:=edit1.text+'9';
end;

procedure TCalculadora.Button4Click(Sender: TObject);
begin
  edit1.clear;
end;

procedure TCalculadora.Button5Click(Sender: TObject);
begin
  edit1.Text:=edit1.text+'4';
end;

procedure TCalculadora.Button6Click(Sender: TObject);
begin
  edit1.Text:=edit1.text+'5';
end;

procedure TCalculadora.Button7Click(Sender: TObject);
begin
  edit1.Text:=edit1.text+'6';
end;

procedure TCalculadora.Button9Click(Sender: TObject);
begin
  edit1.Text:=edit1.text+'2';
end;

end.

