unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckGroup1: TCheckGroup;
    Edit1: TEdit;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioGroup1: TRadioGroup;
    procedure Button1Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    var ve, desc, vt, extra: real;
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin
    ve:= strtofloat(edit1.text);

    if radiobutton1.checked=true then
    desc:=ve*0.10
    else if radiobutton2.checked=true then
    desc:=ve*0.15
    else desc:=ve*0.20;
    Label4.caption:=floattostr(desc);
    vt:= ve-desc;

    if CheckBox1.checked=true then
    extra:= extra+ 50 ;
    if CheckBox2.checked=true then
    extra:= extra+ 75 ;
    if CheckBox3.checked=true then
    extra:= extra+ 100 ;

    extra:= extra+vt;

    Label6.caption:=floattostr(extra);
end;
end.

