unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckGroup1: TCheckGroup;
    Edit1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioGroup1: TRadioGroup;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    var idade, a, extra, valpag, b : double;
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin
 idade:=strtofloat (edit1.text);
 if idade<=18 then
 label8.caption:='Sub-18'
 else if (idade<=40) then
 label8.caption:='Sénior'
 else if (idade>40) then
 label8.caption:='Veteranos';

 if radiobutton1.checked=true then
 label6.caption:='Masculino';
 if radiobutton2.checked=true then
 label6.caption:='Feminino';

 if checkbox1.checked=true then
 label7.caption:='Maratona Porto';
 if checkbox2.checked=true then
 label7.caption:='Maratona Vila-Real';
 if checkbox3.checked=true then
 label7.caption:='Maratona Algarve';

   b:=strtofloat(edit1.text);
   if radiobutton1.checked=true then
   a:=5
   else if radiobutton2.checked=true then
   a:=4 ;

  if checkbox1.checked=true then
  extra:=extra+3;
  if checkbox2.checked=true then
  extra:=extra+2;
  if checkbox3.checked=true then
  extra:=extra+1;
  valpag:=a+extra;
  label9.caption:=floattostr(valpag);
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  application.terminate;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  edit1.clear;
end;

end.

