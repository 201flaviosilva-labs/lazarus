unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Label1: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;
  numero:array[1..4,1..3] of integer;
  x1, z1, num1, num2:integer;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin
  for x1:=1 to 4 do
  begin
    for z1:=1 to 3 do
    begin
      numero[x1,num1]:=strtoint(inputbox('Informação', 'Escolha', '0'));
    end;
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
    num1:=strtoint(inputbox('Informação', 'Escolha a disciplina:', '0'));
    num2:=strtoint(inputbox('Informação', 'Escolha a Nota: ', '0'));
    label1.caption:=inttostr(numero[num1,num2]);

end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  application.terminate;
end;

end.

