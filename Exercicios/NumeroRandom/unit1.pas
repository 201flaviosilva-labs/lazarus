unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioGroup1: TRadioGroup;
    Timer1: TTimer;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Label3MouseEnter(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    var num, aposta, tenta, nivel, tempo :integer;
  var  tentatexto :string;
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin

  if radiobutton1.checked=true then
  nivel:=10
  else if radiobutton2.checked=true then
  nivel:=100
  else if radiobutton3.checked=true then
  nivel:=1000;

   randomize;
   num:=random(nivel) + 1;

  tenta:=0;
  repeat
   tenta:= tenta + 1;

  aposta:=strtoint(inputbox('Janela de Apostas', 'Aposta', ' '));

    if num>aposta then
    application.messagebox('0 numero escondido é MAIOR!', 'Informação');

     if num<aposta then
    application.messagebox('0 numero escondido é MENOR!', 'Informação');

  until aposta = num;
  tentatexto:= inttostr (tenta);
  showmessage('Prabéns acretou no número, o seu número de tentativa ' + tentatexto);



end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Application.terminate;
end;

procedure TForm1.Label3MouseEnter(Sender: TObject);
begin
  Label3. caption:= 'Presunto Silva';
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  tempo:=tempo+1;
  label1.Caption:= inttostr (tempo);
  if tempo=1 then
  application.messagebox('O objetivo deste jogo é escolher um nivel de entre os disponiveis á direita, e depois é tentar adivinhar ele, boa sorte!', 'Informação');
end;

end.

