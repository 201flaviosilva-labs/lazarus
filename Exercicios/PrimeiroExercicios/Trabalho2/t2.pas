unit T2;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Label1: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Label1MouseEnter(Sender: TObject);
    procedure Label1MouseLeave(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin
  Label1.caption:= 'aula 1';
end;

procedure TForm1.Label1MouseEnter(Sender: TObject);
begin
   Label1.caption:= 'o cursor está aqui';
end;

procedure TForm1.Label1MouseLeave(Sender: TObject);
begin
  Label1.caption:= 'Label 1';
end;

end.

