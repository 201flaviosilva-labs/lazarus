unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls, Types;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
      private
    { private declarations }
  public
    { public declarations }
    var th, nh, eh, d, s, sf, vd, sl, sf2:double;
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }


procedure TForm1.Button1Click(Sender: TObject);
begin
  s:= strtofloat(edit1.text);
  nh:= strtofloat(edit2.text);
  eh:= strtofloat(edit4.text);
  d:= strtofloat(edit3.text);

  th:=nh*eh;
  sf:=s+th;
  sf2:=sf*d;

  sl:=sf-d;

  label8.caption :=floattostr(sf2);
  Label6.caption:=floattostr(sl);

end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  edit1.clear;
  edit2.clear;
  edit3.clear;
  edit4.clear;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  application.terminate;
end;

end.

