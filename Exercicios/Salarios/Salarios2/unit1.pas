unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioGroup1: TRadioGroup;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    var sb, nh, nhe, descv, desc, salarioextra, sl: double;
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button2Click(Sender: TObject);
begin
  edit1.clear;
  edit2.clear;
  edit3.clear;

end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  sb:= strtofloat (edit1.text);
  nh:= strtofloat (edit2.text);
  nhe:= strtofloat (edit3.text);


  if  radiobutton1:=true then
  desc:=0.11;
  if  radiobutton2:=true then
  desc:=0.15;
  if  radiobutton3:=true then
  desc:=0.21;

  salarioextra:=sb+nhe*nh;
  descv:=desc*salarioextra;
  sl:=salarioextra-descv;

   label4.caption:=sl;


end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  Application.terminate;
end;


end.

