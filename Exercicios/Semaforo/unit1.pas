unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Panel1: TPanel;
    Shape1: TShape;
    Shape2: TShape;
    Shape3: TShape;
    Timer1: TTimer;
    procedure Timer1Timer(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    seg: integer;
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Timer1Timer(Sender: TObject);
begin
   seg:=seg+1;
   if seg:=12 then
   seg:=0;

   if seg<6 then
   begin
     shape1.visible:=true;
     shape2.visible:=false;
     shape3.visible:=false;
   end;

   if seg =7 then
   begin
     shape1.visible:=false;
     shape2.visible:=true;
     shape3.visible:=false;

   end;


   if seg>7 then
   begin
     shape1.visible:=false;
     shape2.visible:=false;
     shape3.visible:=true;
   end;




end;

end.

