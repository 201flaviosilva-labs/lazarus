unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Panel1: TPanel;
    Shape1: TShape;
    Shape2: TShape;
    Shape3: TShape;
    Timer1: TTimer;
    procedure Timer1Timer(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;
  seg:integer;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  seg:=seg+1;
   if seg=30 then
   seg:=0;

   if seg<10 then
   begin
     shape1.visible:=true;
     shape2.visible:=false;
     shape3.visible:=false;
     label2.Caption:=('Pare');
   end;

   if ((seg>10) and (seg<15)) or (seg>25) then
   begin
     shape1.visible:=false;
     shape2.visible:=true;
     shape3.visible:=false;
      label2.Caption:=('Tenha cuidado');
   end;


   if (seg>15) and (seg<25) then
   begin
     shape1.visible:=false;
     shape2.visible:=false;
     shape3.visible:=true;
     label2.Caption:=('Avançe');
   end;
end;

end.

