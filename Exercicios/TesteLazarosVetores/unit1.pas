unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    ListBox1: TListBox;
    ListBox2: TListBox;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }

  end;

var
  Form1: TForm1;
  equipa1:array [1..5] of integer;
  equipa2:array [1..5] of integer;
  x1, x2, x3, x4, x5, soma1, soma2, maximo1, minimo1, maximo2, minimo2, maximo3, minimo3:integer;
  media1, media2:double;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin
   for x1:= 1 to 5 do
   begin
     equipa1[x1]:=strtoint(inputbox('Informação', 'Diga o Número do Peso do ' + inttostr (x1) + 'º Jogador','0'));
    listbox1.items.add(inttostr(equipa1[x1]));
    end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
   for x2:= 1 to 5 do
   begin
     equipa2[x2]:=strtoint(inputbox('Informação', 'Diga o Número do Peso do ' + inttostr (x2) + 'º Jogador','0'));
    listbox2.items.add(inttostr(equipa2[x2]));
    end;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  application.terminate;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  //equipa 1
  maximo1:=1000;
  minimo1:=0;
  for x3:=1 to 5 do
  begin
   soma1:= equipa1[x3]+soma1;
   //fazer a soma
   if equipa1[x3]< maximo1 then
   maximo1:=equipa1[x3];
   if equipa1[x3]> minimo1 then
   minimo1:=equipa1[x3];
   ///encontar o mais pesado e o mais leve
  end;
  media1:=soma1/5;
  label11.caption:= floattostr(media1);
  label12.caption:= inttostr(maximo1);
  label10.caption:= inttostr(minimo1);
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  //equipa 2
  maximo2:=1000;
  minimo2:=0;
    for x4:=1 to 5 do
  begin
   //fazer a soma
   soma2:= equipa2[x4]+soma2;
   ///encontar o mais pesado e o mais leve
   if equipa2[x4]< maximo2 then
   maximo2:=equipa2[x4];
   if equipa2[x4]> minimo2 then
   minimo2:=equipa2[x4];

  end;
  media2:=soma2/5;
  label14.caption:=floattostr(media2);
  label15.caption:= inttostr(maximo2);
  label13.caption:= inttostr(minimo2);
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
  maximo3:=0;
  minimo3:=1000;
  //encontar o mais pesado do jogo
  for x5:=1 to 5 do
  begin
   if equipa1[x5]> maximo3 then
   maximo3:=equipa1[x5];
   if equipa2 [x5]> maximo3 then
   maximo3:=equipa2[x5];
   label16.caption:=inttostr(maximo3);
  end;
end;

procedure TForm1.Button7Click(Sender: TObject);
begin
  listbox1.items.clear;
  listbox2.items.clear;
  label10.Caption:='0';
  label11.Caption:='0';
  label12.Caption:='0';
  label13.Caption:='0';
  label15.Caption:='0';
  label16.Caption:='0';
  media1:=0;
  media2:=0;
end;

end.

