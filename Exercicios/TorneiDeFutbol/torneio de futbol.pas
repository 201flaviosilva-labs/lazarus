unit TORNEIO_DE_FUTBOL;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Edit1: TEdit;
    Edit10: TEdit;
    Edit11: TEdit;
    Edit12: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Edit8: TEdit;
    Edit9: TEdit;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label2: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);


  private
    { private declarations }
  public
    { public declarations }
    var j1, a1, b1, c1, j2, b2, a2, c2, j3, c3, a3, b3, pontosj, pontosa, pontosb, pontosc: double;
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin

  j1:=strtofloat(edit1.text);
  a1:=strtofloat(edit2.text);
  b1:=strtofloat(edit7.text);
  c1:=strtofloat(edit10.text);


  if j1>a1 then
  pontosj:= pontosj+3
  else if j1=a1 then
  pontosj:=pontosj +1;

  label11.Caption:=(floattostr(pontosj));

  if j1<a1 then
  pontosa:= pontosa+3
  else if j1=a1 then
  pontosa:=pontosa +1;

  label20.Caption:=(floattostr(pontosa));

  if b1>c1 then
  pontosb:= pontosb+3
  else if b1=c1 then
  pontosb:=pontosb +1;

  label18.Caption:=(floattostr(pontosb));

  if b1<c1 then
  pontosc:= pontosc+3
  else if b1=c1 then
  pontosc:=pontosc +1;

  label19.Caption:=(floattostr(pontosc));

end;

procedure TForm1.Button2Click(Sender: TObject);
begin

    j2:=strtofloat(edit4.text);
    a2:=strtofloat(edit11.text);
    b2:=strtofloat(edit3.text);
    c2:=strtofloat(edit8.text);


    if j2>b2 then
    pontosj:= pontosj+3
    else if j2=b2 then
    pontosj:=pontosj +1;

    label11.Caption:=(floattostr(pontosj));

    if c2<a2 then
    pontosa:= pontosa+3
    else if c2=a2 then
    pontosa:=pontosa +1;

    label20.Caption:=(floattostr(pontosa));

    if b2>j2 then
    pontosb:= pontosb+3
    else if b2=j2 then
    pontosb:=pontosb +1;

    label18.Caption:=(floattostr(pontosb));

    if a2<c2 then
    pontosc:= pontosc+3
    else if a2=c2 then
    pontosc:=pontosc +1;

    label19.Caption:=(floattostr(pontosc));
end;

procedure TForm1.Button3Click(Sender: TObject);
begin

  j3:=strtofloat(edit5.text);
  a3:=strtofloat(edit9.text);
  b3:=strtofloat(edit12.text);
  c3:=strtofloat(edit6.text);


  if j3>c3 then
  pontosj:= pontosj+3
  else if j3=c3 then
  pontosj:=pontosj +1;

  label11.Caption:=(floattostr(pontosj));

  if b3<a3 then
  pontosa:= pontosa+3
  else if b3=a3 then
  pontosa:=pontosa +1;

  label20.Caption:=(floattostr(pontosa));

  if b3>a3 then
  pontosb:= pontosb+3
  else if b3=a3 then
  pontosb:=pontosb +1;

  label18.Caption:=(floattostr(pontosb));

  if j3<c3 then
  pontosc:= pontosc+3
  else if j3=c3 then
  pontosc:=pontosc +1;

  label19.Caption:=(floattostr(pontosc));
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
    label11.caption:='PONTOS 10ºJ';
    label18.caption:='PONTOS 10ºB';
    label19.caption:='PONTOS 10ºC';
    label20.caption:='PONTOS 10ºA';
    edit1.Clear;
    edit2.Clear;
    edit3.Clear;
    edit4.Clear;
    edit5.Clear;
    edit6.Clear;
    edit7.Clear;
    edit8.Clear;
    edit9.Clear;
    edit10.Clear;
    edit11.Clear;
    edit12.Clear;
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
 Application.Terminate;
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
  if (pontosj>pontosb) and (pontosj>pontosc) and (pontosj>pontosa) then
   label21.caption:= 'Equipa j venceu';
  if (pontosa>pontosb) and (pontosa>pontosc) and (pontosa>pontosj) then
   label21.caption:= 'Equipa A venceu';
  if (pontosb>pontosj) and (pontosb>pontosc) and (pontosb>pontosa) then
   label21.caption:= 'Equipa B venceu';
  if (pontosc>pontosb) and (pontosc>pontosj) and (pontosc>pontosa) then
   label21.caption:= 'Equipa C venceu';
end;


end.
