unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Label1: TLabel;
    ListBox1: TListBox;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;
  dados:array[1..5] of integer;
  x, dadosmaior: integer;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin
  for x:=1 to 5 do
  begin
  dados[x]:=strtoint(inputbox('Janela', 'informação','0'));
  listbox1.items.add(inttostr(dados[x]));

  if dadosmaior<dados[x] then
  label1.caption:= (dadosmaior);
  end;


end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  application.terminate;
end;

end.

