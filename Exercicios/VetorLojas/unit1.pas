unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    ListBox1: TListBox;
    ListBox2: TListBox;
    ListBox3: TListBox;
    ListBox4: TListBox;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Label4MouseEnter(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;
    dados1:array[1..4] of integer;
    dados2:array[1..4] of integer;
    soma:array[1..4] of integer;
    objetivo:array [1..4] of integer;
    num1, num2, num3, num4, soma2: integer;
    media:double;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin
  for num1:=1 to 4 do
  begin
    dados1[num1]:=strtoint(inputbox('Informação', 'Diga um Número','0'));
    listbox1.items.add(inttostr(dados1[num1]));

  end;
  end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  for num2:=1 to 4 do
  begin
    dados2[num2]:=strtoint(inputbox('Informação', 'Diga um Número','0'));
    listbox2.items.add(inttostr(dados2[num2]));
  end;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  for num3:=1 to 4 do
  begin
   soma[num3]:= dados1[num3]+dados2[num3];
    listbox3.items.add(inttostr (soma[num3]));
  end;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  application.terminate;
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  listbox1.items.clear;
  listbox2.items.clear;
  listbox3.items.clear;
  listbox4.items.clear;
  label7.Caption:='0';

end;

procedure TForm1.Button6Click(Sender: TObject);
begin
  for num4:=1 to 4 do
  begin
    objetivo[num4]:=soma[num4]*2;
    listbox4.Items.add(inttostr(objetivo[num4]));

  soma2:=objetivo[num4]+soma2;
  media:=soma2/4;
  end;
  label7.caption:=floattostr(media);
end;

procedure TForm1.Label4MouseEnter(Sender: TObject);
begin
  label4.caption:='Presunto Silva';
end;

end.

