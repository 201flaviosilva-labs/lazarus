unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    dados:array [1..100] of integer;
  end;

var
  Form1: TForm1;
  num, numver:integer;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin
  randomize;
  for num:=1 to 100 do
  begin
  dados[num]:=random (1000);
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  numver:=strtoint(inputbox('Informação', 'Diga um Número','0'));
  label1.caption:=inttostr(dados[numver]);
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  application.terminate;
end;

end.

