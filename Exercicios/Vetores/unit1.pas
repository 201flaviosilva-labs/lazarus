unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Label1: TLabel;
    Label2: TLabel;
    ListBox1: TListBox;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;
  nomes:array [1..5] of string;
  x:integer;
  consulta: integer;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin
  nomes [1]:= 'António';
  nomes [2]:= 'Bruno';
  nomes [3]:= 'Carlos';
  nomes [4]:= 'Dionísio';
  nomes [5]:= 'Fernando';
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  label1.caption:= (nomes [3]);
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  listbox1.items.clear;

  for x:= 1 to 5 do
    begin
    listbox1.items.add(nomes[x]);
    end;
    end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  consulta:=strtoint(inputbox('Informação', 'Escolha', ''));
  label2.caption:=(nomes[consulta]);
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  application.terminate;
end;


end.

