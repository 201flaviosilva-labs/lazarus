unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Timer1: TTimer;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;
  nomeempresa, Nome1produto:string;
  tempo, nivel1produto:integer;
  dinheiroacumulado:integer;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  tempo:=tempo+1;
  if tempo=1 then
   begin
     label1.visible:=false;
     Label2.visible:=false;
     Label3.visible:=false;
     label4.visible:=false;
     label5.visible:=false;
     label6.visible:=false;
     button2.visible:=false;
     button3.Visible:=false;
     button4.Visible:=false;

   end;

end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  button1.visible:=false;
  application.messagebox('Vamos então por começar com os objetivos deste jogo. O objetivo deste jogo é simular uma gestão real de uma micro Empresa, como gerenciar os funcionários, os seus produtos, entre outros. Mas para começar, vamos por escolher o nome da Empresa!', 'Informação de jogo');
  label1.Visible:=true;
  button2.visible:=true;
  Nomeempresa:=(inputbox('Informação', 'Escolha o nome para a sua Empresa', ''));
  label1.caption:=(nomeempresa);
  application.messagebox('Bonito nome para uma Empresa, de certeza que com esse nome irá fazer muito sucesso! E agora vamos decidir qual é o primeiro produto que vamos por á venda.', 'Informação de jogo');
  Nome1produto:=(inputbox('Informação', 'Escolha o nome para o seu primeiro produto de venda', ''));
  Label2.visible:=true;
  Label3.visible:=true;
  label4.visible:=true;
  button3.Visible:=true;
  button4.visible:=true;
  Label3.caption:=(nome1produto);
  application.messagebox('Agora, é hora de se começar a investir na tua empresa! Aumenta o nível de qualidade do teu primeiro produto, é óbvio que isto vai ter custos, mas também não faz mal, tu já começas com um montante cumulado.', 'Informação de jogo');
  dinheiroacumulado:=5000;
  label6.Caption:=floattostr(dinheiroacumulado);
  label5.visible:=true;
  label6.visible:=true;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Application.terminate;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  nivel1produto:=nivel1produto+1;
  label4.caption:=inttostr (nivel1produto);

  if nivel1produto= 10 then
   begin
  label4.caption:='Nivel Máximo';
  button3.visible:=false;
  application.messagebox('Tu não podes ter um Nivel Maior que 10 em um produto!', ' Informação do Jogo');
   end;

  if nivel1produto= 1 then
  button4.Visible:=true;
  if nivel1produto< 1 then
  button4.Visible:=true;

   begin
   //Aqui ainda tenho que fazer que se o utilizador não tiver dinheiro que ele tem ganhar lucros e depois voltar a tentar, ou sja se o saldo for negativo para ele esperar um pouco e depois voltar a tentar;
   //criar label que diga o custo da proxima atualização;
  if nivel1produto=1 then
   dinheiroacumulado:= dinheiroacumulado-1500;
  if nivel1produto=2 then
   dinheiroacumulado:= dinheiroacumulado-3000;
  if nivel1produto=3 then
   dinheiroacumulado:= dinheiroacumulado-4500;
  if nivel1produto=4 then
   dinheiroacumulado:= dinheiroacumulado-6500;
  if nivel1produto=5 then
   dinheiroacumulado:= dinheiroacumulado-8000;
  if nivel1produto=6 then
   dinheiroacumulado:= dinheiroacumulado-9000;
  if nivel1produto=7 then
   dinheiroacumulado:= dinheiroacumulado-10500;
  if nivel1produto=8 then
   dinheiroacumulado:= dinheiroacumulado-12000;
  if nivel1produto=9 then
   dinheiroacumulado:= dinheiroacumulado-13500;
  if nivel1produto=10 then
   dinheiroacumulado:= dinheiroacumulado-15500;

   end;
   label6.caption:=floattostr (dinheiroacumulado);
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
   nivel1produto:=nivel1produto-1;
  label4.caption:=inttostr (nivel1produto);

   if nivel1produto< 0 then
   begin
  label4.caption:='Nivel Minimo';
  button4.visible:=false;
  application.messagebox('Tu não podes ter um Nivel menor que 0 (zero) em um produto!', ' Informação do Jogo');
   end;

   if nivel1produto= 9 then
  button3.Visible:=true;
   if nivel1produto< 9 then
  button3.Visible:=true;

   begin
  if nivel1produto=0 then
   dinheiroacumulado:= dinheiroacumulado+1000;
  if nivel1produto=1 then
   dinheiroacumulado:= dinheiroacumulado+2000;
  if nivel1produto=2 then
   dinheiroacumulado:= dinheiroacumulado+3000;
  if nivel1produto=3 then
   dinheiroacumulado:= dinheiroacumulado+4000;
  if nivel1produto=4 then
   dinheiroacumulado:= dinheiroacumulado+5000;
  if nivel1produto=5 then
   dinheiroacumulado:= dinheiroacumulado+6000;
  if nivel1produto=6 then
   dinheiroacumulado:= dinheiroacumulado+7000;
  if nivel1produto=7 then
   dinheiroacumulado:= dinheiroacumulado+8000;
  if nivel1produto=8 then
   dinheiroacumulado:= dinheiroacumulado+9000;
  if nivel1produto=9 then
   dinheiroacumulado:= dinheiroacumulado+10000;
   end;
   label6.caption:=floattostr (dinheiroacumulado);

end;

end.

